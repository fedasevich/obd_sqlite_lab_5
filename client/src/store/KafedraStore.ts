import { action, makeAutoObservable } from "mobx";
import { fetchKafedras } from "../http/kafedraApi";

export type KafedraData = {
    id?: number;
    name?: string;
    telefone?: string;
}


export default class KafedraStore {
    _kafedras: Array<KafedraData>
    constructor() {
        this._kafedras = []
        makeAutoObservable(this)
    }

    setKafedra(kafedras: KafedraData[]) {
        this._kafedras = kafedras
    }

    get kafedras() {
        return this._kafedras
    }

    loadKafedras() {
        return fetchKafedras().then(action((kafedras: KafedraData[]) => {
            this._kafedras = kafedras
        }))
    }
}
