import { action, makeAutoObservable } from "mobx";
import { fetchVykladachs } from "../http/vykladachApi";

export type VykladachData = {
    id: number;
    name: string;
    lastName: string;
    middleName: string;
    fullName: string;
    nomPost: number;
    nomKaf: number;
}


export default class VykladachStore {
    _vykladachs: Array<VykladachData>
    constructor() {
        this._vykladachs = []
        makeAutoObservable(this)
    }

    setVykladach(vykladachs: VykladachData[]) {
        this._vykladachs = vykladachs
    }

    get vykladachs() {
        return this._vykladachs
    }

    getVykladachById(id: number) {
        return this._vykladachs.find(vykladach => vykladach.id === id)
    }

    getVykladachIdByFullName(fullName: string) {
        const vykladach = this._vykladachs.find(vykladach => vykladach.fullName === fullName)
        if (!vykladach) {
            return 0
        }
        return vykladach.id
    }

    loadVykladachs() {
        return fetchVykladachs().then(action((vykladachs: VykladachData[]) => {
            this._vykladachs = vykladachs
        }))
    }
}
