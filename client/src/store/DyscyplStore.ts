import { action, makeAutoObservable } from "mobx";
import { fetchDyscypls } from "../http/dyscyplApi";

export type DyscyplData = {
    id: number;
    name: string;
}


export default class DyscyplStore {
    _dyscypls: Array<DyscyplData>
    constructor() {
        this._dyscypls = []
        makeAutoObservable(this)
    }

    setDyscypl(dyscypls: DyscyplData[]) {
        this._dyscypls = dyscypls
    }

    get dyscypls() {
        return this._dyscypls
    }

    getDyscyplById(id:number) {
        return this._dyscypls.find(dyscypl => dyscypl.id === id)
    }

    getDyscyplIdByName(name:string) {
        const dyscypl = this._dyscypls.find(dyscypl => dyscypl.name === name)
        if (!dyscypl) {
            return 0
        }
        return dyscypl.id
    }

    loadDyscypls() {
        return fetchDyscypls().then(action((dyscypls: DyscyplData[]) => {
            this._dyscypls = dyscypls
        }))
    }
}
