import { action, makeAutoObservable } from "mobx";
import { fetchStudGrups } from "../http/studGrupApi";

export type StudGrupData = {
    id: number;
    nomer: string;
    studAmount: string;
}


export default class StudGrupStore {
    _studGrups: Array<StudGrupData>
    constructor() {
        this._studGrups = []
        makeAutoObservable(this)
    }

    setStudGrup(studGrups: StudGrupData[]) {
        this._studGrups = studGrups
    }

    get studGrups() {
        return this._studGrups
    }

    loadStudGrups() {
        return fetchStudGrups().then(action((studGrups: StudGrupData[]) => {
            this._studGrups = studGrups
        }))
    }
}
