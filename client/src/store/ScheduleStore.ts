import { action, makeAutoObservable } from "mobx";
import { createGroupSchedule, deleteSchedule, editGroupSchedule, fetchGroupSchedule } from "../http/scheduleApi";

export type ScheduleData = {
    id: number,
    weekDay: number,
    classNumber: number,
    dyscyplId: number,
    studGrupId: number,
    vykladachId: number,
    createdAt: string,
    updatedAt: string
}


export default class ScheduleStore {
    _schedules: ScheduleData[]
    constructor() {
        this._schedules = [
            { id: 524, weekDay: 1, classNumber: 1, createdAt: "2023-03-08T14:22:44.000Z", updatedAt: "2023-03-08T14:22:44.000Z", dyscyplId: 666, studGrupId: 107, vykladachId: 50 },
        ]
        makeAutoObservable(this)
    }

    setSchedule(schedules: ScheduleData[]) {
        this._schedules = schedules
    }

    get schedule() {
        console.error("trigger", this._schedules)
        return this._schedules
    }

    getGroupScheduletestFunc() {
        return this._schedules
    }

    sortSchedules() {
        this._schedules = this._schedules.sort((a, b) => {
            return a.weekDay - b.weekDay || a.classNumber - b.classNumber;
        });
    }

    loadSchedules(studGrupId: number) {
        return fetchGroupSchedule(studGrupId).then(action((schedules: ScheduleData[]) => {
            this._schedules = schedules
            this.sortSchedules()
        }))
    }


    createSchedule(weekDay: number, selectedGroup: number, selectedDyscyplId: number, selectedVykladachId: number, selectedClassNumber: number) {
        createGroupSchedule(weekDay, selectedGroup, selectedDyscyplId, selectedVykladachId, selectedClassNumber).then(action((newSchedule: ScheduleData) => {
            if (newSchedule) {
            console.log(newSchedule)
            this._schedules.push(newSchedule)
            this.sortSchedules()
            }
        }))
    }

    findScheduleById(id: number) {
        return this._schedules.find(schedule => schedule.id === id)
    }

    changeSchedule(weekDay: number, selectedGroup: number, selectedDyscyplId: number, selectedVykladachId: number, selectedClassNumber: number, id: number) {
        editGroupSchedule(weekDay, selectedGroup, selectedDyscyplId, selectedVykladachId, selectedClassNumber, id).then(action(() => {
            const schedule = this.findScheduleById(id)
            if (schedule) {
                schedule.dyscyplId = selectedDyscyplId
                schedule.weekDay = weekDay
                schedule.classNumber = selectedClassNumber
                schedule.vykladachId = selectedVykladachId
            }
        }))
    }

    removeSchedule(id: number) {
        deleteSchedule(id).then(action(() => {
            const scheduleIndex = this._schedules.findIndex(schedule => schedule.id === id)
            this._schedules.splice(scheduleIndex, 1)
        }))
    }
}

