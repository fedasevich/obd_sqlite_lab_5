import React, { createContext } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';
import KafedraStore from './store/KafedraStore';
import DyscyplStore from './store/DyscyplStore';
import StudGrupStore from './store/StudGrupStore';
import VykladachStore from './store/VykladachStore';
import ScheduleStore from './store/ScheduleStore';

export const Context = createContext<{ kafedra: KafedraStore, dyscypl: DyscyplStore, studGrup: StudGrupStore, vykladach: VykladachStore, schedule: ScheduleStore }>({
  kafedra: new KafedraStore(),
  dyscypl: new DyscyplStore(),
  studGrup: new StudGrupStore(),
  vykladach: new VykladachStore(),
  schedule: new ScheduleStore(),
});

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <Context.Provider value={{
    kafedra: new KafedraStore(),
    dyscypl: new DyscyplStore(),
    studGrup: new StudGrupStore(),
    vykladach: new VykladachStore(),
    schedule: new ScheduleStore(),
  }}>
    <App />
  </Context.Provider>
);
