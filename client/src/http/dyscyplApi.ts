import { DyscyplData } from '../store/DyscyplStore';
import { $host } from './index';

export const fetchDyscypls = async (): Promise<DyscyplData[]> => {
    const { data } = await $host.get('dyscypl');
    return data;
};
