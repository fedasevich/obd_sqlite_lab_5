import { KafedraData } from '../store/KafedraStore';
import { $host } from './index';

export const fetchKafedras = async (): Promise<KafedraData[]> => {
    const { data } = await $host.get('kafedra');
    return data;
};
