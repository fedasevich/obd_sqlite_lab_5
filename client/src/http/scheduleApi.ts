import { ScheduleData } from '../store/ScheduleStore';
import { $host } from './index';

export const fetchGroupSchedule = async (studGrupId: number): Promise<ScheduleData[]> => {
    const { data } = await $host.get(`schedule/${studGrupId}`);
    return data;
};


export const createGroupSchedule = async (weekDay: number, studGrupId: number, dyscyplId: number, vykladachId: number, classNumber: number) => {
    try {
        const { data } = await $host.post(`schedule`, { weekDay, studGrupId, dyscyplId, vykladachId, classNumber });
        return data;
    } catch (error: any) {
        alert(error.response.data.message)
    }
};
export const editGroupSchedule = async (weekDay: number, studGrupId: number, dyscyplId: number, vykladachId: number, classNumber: number, id: number) => {
    try {
        const { data } = await $host.put(`schedule/${id}`, { weekDay, studGrupId, dyscyplId, vykladachId, classNumber });
        return data;
    } catch (error: any) {
        alert(error.response.data.message)
    }
};


export const deleteSchedule = async (id: number) => {
    try {
        const { data } = await $host.delete(`schedule/${id}`);
        return data;
    } catch (error: any) {
        alert(error.response.data.message)
    }
};
