import { StudGrupData } from '../store/StudGrupStore';
import { $host } from './index';

export const fetchStudGrups = async (): Promise<StudGrupData[]> => {
    const { data } = await $host.get('studGrup');
    return data;
};
