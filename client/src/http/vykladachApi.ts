import { VykladachData } from '../store/VykladachStore';
import { $host } from './index';

export const fetchVykladachs = async (): Promise<VykladachData[]> => {
    const { data } = await $host.get('vykladach');
    return data;
};
