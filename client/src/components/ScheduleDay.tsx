import React, { ReactElement } from 'react'
import { observer } from 'mobx-react-lite'
import { Col } from 'react-bootstrap';

interface ScheduleDayProps {
    weekDay: number,
    children: React.ReactNode[] | React.ReactNode
}

export const ScheduleDay: React.FC<ScheduleDayProps> = observer(({ weekDay, children }) => {
    const weekDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday",];
    return (

        <Col md={3} className="mb-5">
            <div className="weekDay">
                <h2>{weekDays[weekDay - 1]}</h2>
                <hr />
                {children}
            </div>
        </Col>


    );
})
