import React, { useContext, useState } from 'react'
import { observer } from 'mobx-react-lite'
import { Context } from '..';
import { ClassModal } from './ClassModal';

interface AddClassButtonProps {
    weekDay: number,
    selectedGroup: number
}

export const AddClassButton: React.FC<AddClassButtonProps> = observer(({ weekDay, selectedGroup }) => {
    const [showModal, setShowModal] = useState(false);
    const { vykladach, dyscypl, schedule } = useContext(Context)


    const handleShow = () => setShowModal(true);
    return (
        <>
            <button className="addClassButton" onClick={handleShow}>
                +
            </button>

            <ClassModal weekDay={weekDay}
            vykladach={vykladach}
            dyscypl={dyscypl}
            show={showModal}
            setShow={setShowModal}
            selectedGroup={selectedGroup}
            schedule={schedule}
            />
        </>
    )
})
