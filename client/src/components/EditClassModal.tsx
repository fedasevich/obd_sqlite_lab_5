import React, { useContext, useEffect, useState } from 'react'
import { observer } from 'mobx-react-lite'
import { Button, Form, Modal } from 'react-bootstrap';
import { Context } from '..';
import { Typeahead } from 'react-bootstrap-typeahead';
import VykladachStore, { VykladachData } from '../store/VykladachStore';
import { Option } from 'react-bootstrap-typeahead/types/types';
import DyscyplStore, { DyscyplData } from '../store/DyscyplStore';
import ScheduleStore from '../store/ScheduleStore';
import { DeleteIcon } from '../ui/icons';

interface EditClassModalProps {
    weekDay: number,
    vykladach: VykladachStore,
    dyscypl: DyscyplStore,
    show: boolean,
    setShow: React.Dispatch<React.SetStateAction<any>>,
    selectedGroup: number,
    classNumber: number,
    id: number,
    dyscyplId: number,
    vykladachId: number,
    schedule:ScheduleStore
}

export const EditClassModal: React.FC<EditClassModalProps> = observer(({ weekDay, vykladach, dyscypl, show, setShow, selectedGroup, classNumber, id, dyscyplId, vykladachId, schedule }) => {
    const [selectedVykladachId, setSelectedVykladachId] = useState<number>(0)
    const [selectedDyscyplId, setSelectedDyscyplId] = useState<number>(0)
    const [selectedClassNumber, setSelectedClassNumber] = useState<number>(1)
    const handleClose = () => {
        setShow(false)
    }

    useEffect(() => {
        setSelectedDyscyplId(dyscyplId)
        setSelectedVykladachId(vykladachId)
    }, [dyscyplId, vykladachId])


    const handleSelectVykladach = (selected: Option[]) => {
        setSelectedVykladachId(vykladach.getVykladachIdByFullName(selected.toString()))
    }

    const handleSelectDyscypl = (selected: Option[]) => {
        setSelectedDyscyplId(dyscypl.getDyscyplIdByName(selected.toString()))
    }

    const handleSelectClassNumber = (selected: string) => {
        setSelectedClassNumber(Number(selected))
    }


    const dyscyplOptions = dyscypl.dyscypls.map((dyscypl) => {
        return dyscypl.name
    })

    const vykladachOptions = vykladach.vykladachs.map((vykladach) => {
        return vykladach.fullName
    })

    const handleCommit = () => {
    schedule.changeSchedule(weekDay, selectedGroup, selectedDyscyplId, selectedVykladachId, selectedClassNumber, id)
     handleClose()
    }


    const handleDeleteClick = () => {
        schedule.removeSchedule(id)
        handleClose()
    }
    console.log(selectedVykladachId, selectedDyscyplId, selectedClassNumber)

    return (
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Add class to {weekDay}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Label htmlFor="vykladachSelect">Choose vykladach:</Form.Label>
                    <Typeahead
                        id="vykladachSelect"
                        onChange={handleSelectVykladach}
                        options={vykladachOptions}
                        placeholder="Choose a vykladach..."
                    />
                    <Form.Label htmlFor="classSelect">Choose dyscypl:</Form.Label>
                    <Typeahead
                        id="classSelect"
                        onChange={handleSelectDyscypl}
                        options={dyscyplOptions}
                        placeholder="Choose dyscypl..."
                    />
                    <Form.Group className="mb-3">
                        <Form.Label>Class number select</Form.Label>
                        <Form.Select defaultValue={classNumber} onChange={(event) => {
                            handleSelectClassNumber(event.target.value)
                        }} >
                            {Array.from({ length: 6 }, (_, i) => i + 1).map((mapClassNumber) => {
                                return (
                                    <option key={mapClassNumber} >{mapClassNumber}</option>
                                )
                            })}
                        </Form.Select>
                    </Form.Group>
                </Form>
                <hr />
                <h6 onClick={() => handleDeleteClick()} className="text-danger mb-0 btn d-flex align-items-center" ><DeleteIcon/> Delete class</h6>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
                <Button variant="primary" onClick={handleCommit}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    );
})
