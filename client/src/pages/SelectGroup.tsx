import { observer } from 'mobx-react-lite'
import React, { useContext, useState } from 'react'
import { Typeahead } from 'react-bootstrap-typeahead'
import { Option } from 'react-bootstrap-typeahead/types/types'
import { Context } from '..'
import { ScheduleData } from '../store/ScheduleStore'
import { StudGrupData } from '../store/StudGrupStore'


interface SelectGroupProps {
  setSelectedGroup: React.Dispatch<React.SetStateAction<number | undefined>>;
}

const SelectGroup: React.FC<SelectGroupProps> = observer(({ setSelectedGroup }) => {
  const { studGrup } = useContext(Context)


  const options = studGrup.studGrups.map((studGrup: StudGrupData) => {
    return studGrup.id.toString()
  })

  const handleSelect = (selected: Option[]) => {
    setSelectedGroup(Number(selected[0]));
  };

  return (
    <div className="d-flex w-100 h-100 justify-content-center vh-100 align-items-center">
      <Typeahead
        style={{ width: "300px" }}
        id="basic-example"
        onChange={handleSelect}
        options={options}
        placeholder="Choose a group..."
      />
    </div>
  )
})

export default SelectGroup
