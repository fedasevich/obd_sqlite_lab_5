import React, { useContext, useEffect, useState } from 'react'
import { observer } from 'mobx-react-lite'
import { Context } from '..';
import { ScheduleData } from '../store/ScheduleStore';
import { Col, Container } from 'react-bootstrap';
import Row from 'react-bootstrap/esm/Row';
import { ScheduleDay } from '../components/ScheduleDay';
import '../style.css'
import { AddClassButton } from '../components/AddClassButton';
import { SettingsBackgroundIcon } from '../ui/icons';
import { EditClassModal } from '../components/EditClassModal';
interface ScheduleProps {
    selectedGroup: number;
}


export const Schedule: React.FC<ScheduleProps> = observer(({ selectedGroup }) => {
    const [showModal, setShowModal] = useState({ show: false, weekDay: 0, classNumber: 0, id: 0, dyscyplId: 0, vykladachId: 0 });
    const { schedule, dyscypl, vykladach } = useContext(Context)
    const mappedSchedule: Map<number, ScheduleData[]> = new Map()

    useEffect(() => {
        schedule.loadSchedules(selectedGroup)
    }, [])

    Array.from({ length: 7 }, (_, i) => i + 1).forEach(weekDay => mappedSchedule.set(weekDay, []))
    console.log(schedule.schedule)
    schedule.schedule.forEach((obj: ScheduleData) => {
        console.log(obj, obj.weekDay)
        mappedSchedule.get(obj.weekDay)?.push(obj)
    })
console.log("inside")

    console.log(mappedSchedule)
    const handleGearClick = (schedule: ScheduleData) => {
        console.log({ ...schedule })
        setShowModal({ show: true, ...schedule });
    }

    return (
        <Container fluid>
            <Row>
                <Col md={12} className="mb-5">
                    <h1>{selectedGroup} group schedule </h1>
                    <a href="main">Return to main page</a>
                    <hr />
                </Col>
            </Row>
            <Row>
                {mappedSchedule.entries() && Array.from(mappedSchedule.entries()).map(([weekDay, schedules]) =>

                    <ScheduleDay weekDay={weekDay} key={weekDay}>
                        {schedules.map(schedule =>
                            <Row key={schedule.id} className="d-flex flex-row weekDayItem position-relative">
                                <Col md={1} className="weekDayClassNumber align-items-center d-flex justify-content-center">
                                    <p>{schedule.classNumber}</p>
                                </Col>
                                <Col className="text-center">

                                    <h5>{dyscypl.getDyscyplById(schedule.dyscyplId)?.name}</h5>
                                    <p>{vykladach.getVykladachById(schedule.vykladachId)?.fullName}</p>
                                    <span className="position-absolute top-0 start-100 translate-middle p-2 gear" onClick={() => {
                                        handleGearClick(schedule)
                                    }}>
                                        <SettingsBackgroundIcon />
                                    </span>
                                </Col>
                            </Row>
                        )}
                        {schedules.length < 6 && <AddClassButton weekDay={weekDay} selectedGroup={selectedGroup} />}
                    </ScheduleDay>

                )}

                {Array.from(mappedSchedule.entries()).length < 7 && Array.from({ length: 6 }, (_, i) => i + 1).map((weekDay) => {
                    console.log(Array.from(mappedSchedule.entries()))
                                return (
                                    <ScheduleDay weekDay={weekDay} key={weekDay}>
                                    <AddClassButton weekDay={weekDay} selectedGroup={selectedGroup} />
                                </ScheduleDay>
                                )
                            })
                            }
            </Row>

            <EditClassModal dyscypl={dyscypl}
                selectedGroup={selectedGroup}
                setShow={setShowModal}
                show={showModal.show}
                weekDay={showModal.weekDay}
                vykladach={vykladach}
                classNumber={showModal.classNumber}
                id={showModal.id}
                vykladachId={showModal.vykladachId}
                dyscyplId={showModal.dyscyplId}
                schedule={schedule}
            />

        </Container>

    );
})
