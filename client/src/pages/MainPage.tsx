import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { Context } from '..'
import { Schedule } from './Schedule'

import SelectGroup from './SelectGroup'


const MainPage: React.FC = observer(() => {
  const [selected, setSelected] = useState<number | undefined>()
  const { studGrup, vykladach, dyscypl } = useContext(Context)
  useEffect(() => {
    studGrup.loadStudGrups()
    vykladach.loadVykladachs()
    dyscypl.loadDyscypls()
  }, [])
  return (
    <>

      {!selected
        ? <SelectGroup setSelectedGroup={setSelected} />
        : <Schedule selectedGroup={selected} />}


    </>

  )
})


export default MainPage
