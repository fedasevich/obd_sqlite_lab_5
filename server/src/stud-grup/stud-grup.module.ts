import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { StudGrupController } from './stud-grup.controller';
import { StudGrup } from './stud-grup.model';
import { StudGrupService } from './stud-grup.service';

@Module({
  providers: [StudGrupService],
  controllers: [StudGrupController],
  imports: [
    SequelizeModule.forFeature([StudGrup])
  ],
  exports: [StudGrupService]
})
export class StudGrupModule {}
