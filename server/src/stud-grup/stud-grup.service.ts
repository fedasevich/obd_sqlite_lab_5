
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { StudGrupDto } from './dto/stud-grup.dto';

import { StudGrup } from './stud-grup.model';
@Injectable()
export class StudGrupService {
    constructor(@InjectModel(StudGrup) private studGrupRepository: typeof StudGrup) { }
    async createStudGrup(dto: StudGrupDto) {
        const candidate = await this.getStudGrupByValue(dto);
        if (candidate) {
            throw new HttpException({ message: 'Already exist' }, HttpStatus.BAD_REQUEST);
        }
        const studGrup = await this.studGrupRepository.create(dto);
        return studGrup;
    }
    async getStudGrupByValue(dto: StudGrupDto) {
        const studGrup = await this.studGrupRepository.findOne({ where: { ...dto } });
        if (!studGrup) {
            throw new HttpException({ message: 'Wrong data' }, HttpStatus.BAD_REQUEST);
        }
        return studGrup;
    }
    async getAllStudGrups() {
        const studGrups = await this.studGrupRepository.findAll();
        return studGrups;
    }
    updateStudGrup(id: number, dto: StudGrupDto) {
        return StudGrup.update({ ...dto }, { where: { id } });
    }
    remove(id: number) {
        return StudGrup.destroy({ where: { id } });
    }
}
