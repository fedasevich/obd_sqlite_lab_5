import { IsNumber } from "class-validator";
export class StudGrupDto {
    @IsNumber({}, { message: "Must be number" })
    readonly nomer: number;
    @IsNumber({}, { message: "Must be number" })
    readonly studAmount: number;
}
