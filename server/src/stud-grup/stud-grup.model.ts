import { Model, Table, Column, DataType, HasMany } from "sequelize-typescript";
import { Schedule } from "src/schedule/schedule.model";


interface StudGrupCreationAttrs {
    nomer: number;
    studAmount:number;
}

@Table({ tableName: 'stud-grup' })
export class StudGrup extends Model<StudGrup, StudGrupCreationAttrs> {
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;
    @Column({ type: DataType.INTEGER, unique: true })
    nomer: number;
    @Column({ type: DataType.INTEGER })
    studAmount:number;
    @HasMany(() => Schedule)
    schedules:Schedule[];
}
