
import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { StudGrupDto } from './dto/stud-grup.dto';
import { StudGrupService } from './stud-grup.service';

@Controller('studGrup')
export class StudGrupController {
    constructor(private studGrupService: StudGrupService) { }
    @Post()
    create(@Body() StudGrupDto: StudGrupDto) {
        return this.studGrupService.createStudGrup(StudGrupDto);
    }


    @Get('/:studGrup')
    getByValue(@Param() params: StudGrupDto) {
        return this.studGrupService.getStudGrupByValue(params);
    }


    @Get()
    getAll() {
        return this.studGrupService.getAllStudGrups();
    }


    @Put(':id')
    update(@Param('id') id: number, @Body() StudGrupDto: StudGrupDto) {
        return this.studGrupService.updateStudGrup(id, StudGrupDto);
    }


    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.studGrupService.remove(id);
    }
}
