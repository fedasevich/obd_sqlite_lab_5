
import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { VykladachDto } from './dto/vykladach.dto';
import { VykladachService } from './vykaldach.service';


@Controller('vykladach')
export class VykladachController {
    constructor(private vykladachService: VykladachService) { }
    @Post()
    create(@Body() VykladachDto: VykladachDto) {
        return this.vykladachService.createVykladach(VykladachDto);
    }


    @Get('/:vykladach')
    getByValue(@Param() params: VykladachDto) {
        return this.vykladachService.getVykladachByValue(params);
    }


    @Get()
    getAll() {
        return this.vykladachService.getAllVykladachs();
    }


    @Put(':id')
    update(@Param('id') id: number, @Body() VykladachDto: VykladachDto) {
        return this.vykladachService.updateVykladach(id, VykladachDto);
    }


    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.vykladachService.remove(id);
    }
}
