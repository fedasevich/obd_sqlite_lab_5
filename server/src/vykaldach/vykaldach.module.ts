import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { VykladachController } from './vykaldach.controller';
import { VykladachService } from './vykaldach.service';
import { Vykladach } from './vykaldach.model';

@Module({
  controllers: [VykladachController],
  providers: [VykladachService],
  imports: [
    SequelizeModule.forFeature([Vykladach])
  ],
  exports: [VykladachService]
})
export class VykladachModule {}
