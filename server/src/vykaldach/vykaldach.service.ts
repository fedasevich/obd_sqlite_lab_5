
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { VykladachDto } from './dto/vykladach.dto';

import { Vykladach } from './vykaldach.model';
@Injectable()
export class VykladachService {
    constructor(@InjectModel(Vykladach) private vykladachRepository: typeof Vykladach) { }
    async createVykladach(dto: VykladachDto) {
        const candidate = await this.getVykladachByValue(dto);
        if (candidate) {
            throw new HttpException({ message: 'Already exist' }, HttpStatus.BAD_REQUEST);
        }
        const vykladach = await this.vykladachRepository.create(dto);
        return vykladach;
    }
    async getVykladachByValue(dto: VykladachDto) {
        const vykladach = await this.vykladachRepository.findOne({ where: { ...dto } });
        if (!vykladach) {
            throw new HttpException({ message: 'Wrong data' }, HttpStatus.BAD_REQUEST);
        }
        return vykladach;
    }
    async getAllVykladachs() {
        const vykladachs = await this.vykladachRepository.findAll();
        return vykladachs;
    }
    updateVykladach(id: number, dto: VykladachDto) {
        return Vykladach.update({ ...dto }, { where: { id } });
    }
    remove(id: number) {
        return Vykladach.destroy({ where: { id } });
    }
}
