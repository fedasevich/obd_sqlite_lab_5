import { Model, Table, Column, DataType, ForeignKey, HasMany } from "sequelize-typescript";
import { Kafedra } from "src/kafedra/kafedra.model";
import { Posada } from "src/posada/posada.model";
import { Schedule } from "src/schedule/schedule.model";


interface VykladachCreationAttrs {
  name: string;
  lastName: string;
  middleName: string;
  kodPost: number;
  nomKaf: number;
}

@Table({ tableName: 'vykladach' })
export class Vykladach extends Model<Vykladach, VykladachCreationAttrs> {
  @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
  id: number;
  @Column({ type: DataType.STRING })
  name: string;
  @Column({ type: DataType.STRING })
  lastName: string;
  @Column({ type: DataType.STRING })
  middleName: string;

  @Column({
    get() {
      const firstName = this.getDataValue('name');
      const lastName = this.getDataValue('lastName');
      const middleName = this.getDataValue('middleName');
      return `${lastName} ${firstName[0]}.${middleName[0]}`;
    }
  })
  fullName: string;

  @ForeignKey(() => Kafedra)
  nomKaf: number;

  @ForeignKey(() => Posada)
  nomPost: number;

  @HasMany(() => Schedule)
  schedules: Schedule[];
}
