import { IsNumber, IsString } from "class-validator";
export class VykladachDto {
    @IsString({ message: "Must be string" })
    readonly name: string;
    @IsString({ message: "Must be string" })
    readonly lastName: string;
    @IsString({ message: "Must be string" })
    readonly middleName: string;
    @IsNumber({}, { message: "Must be number" })
    readonly kodPost:number;
    @IsNumber({}, { message: "Must be number" })
    readonly nomKaf:number;
}
