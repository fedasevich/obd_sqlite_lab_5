import { Model, Table, Column, DataType, HasMany } from "sequelize-typescript";
import { Schedule } from "src/schedule/schedule.model";


interface DyscyplCreationAttrs {
    name: string;
}

@Table({ tableName: 'dyscypl' })
export class Dyscypl extends Model<Dyscypl, DyscyplCreationAttrs> {
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;
    @Column({ type: DataType.STRING, unique: true })
    name: string;
    @HasMany(() => Schedule)
    schedules:Schedule[];
}
