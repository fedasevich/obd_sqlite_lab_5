import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { DyscyplDto } from './dto/dyscypl.dto';
import { Dyscypl } from './dyscypl.model';

@Injectable()
export class DyscyplService {
    constructor(@InjectModel(Dyscypl) private dyscyplRepository: typeof Dyscypl) { }
    async createDyscypl(dto: DyscyplDto) {
        const candidate = await this.getDyscyplByValue(dto);
        if (candidate) {
            throw new HttpException({ message: 'Already exist' }, HttpStatus.BAD_REQUEST);
        }
        const dyscypl = await this.dyscyplRepository.create(dto);
        return dyscypl;
    }
    async getDyscyplByValue(dto: DyscyplDto) {
        const dyscypl = await this.dyscyplRepository.findOne({ where: { name: dto.name } });
        if (!dyscypl) {
            throw new HttpException({ message: 'Wrong data' }, HttpStatus.BAD_REQUEST);
        }
        return dyscypl;
    }
    async getAllDyscypls() {
        const dyscypls = await this.dyscyplRepository.findAll();
        return dyscypls;
    }
    updateDyscypl(id: number, dto: DyscyplDto) {
        return Dyscypl.update({ name: dto.name }, { where: { id } });
    }
    remove(id: number) {
        return Dyscypl.destroy({ where: { id } });
    }
}
