import { Module } from '@nestjs/common';
import { DyscyplService } from './dyscypl.service';
import { DyscyplController } from './dyscypl.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Dyscypl } from './dyscypl.model';

@Module({
  providers: [DyscyplService],
  controllers: [DyscyplController],
  imports: [
    SequelizeModule.forFeature([Dyscypl])
  ],
  exports: [DyscyplService]
})
export class DyscyplModule {}
