import { IsString } from "class-validator";
export class DyscyplDto {
    @IsString({ message: "Must be string" })
    readonly name: string;
}
