import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { DyscyplDto } from './dto/dyscypl.dto';
import { DyscyplService } from './dyscypl.service';

@Controller('dyscypl')
export class DyscyplController {
    constructor(private dyscyplService: DyscyplService) { }
    @Post()
    create(@Body() DyscyplDto: DyscyplDto) {
        return this.dyscyplService.createDyscypl(DyscyplDto);
    }


    @Get('/:dyscypl')
    getByValue(@Param() params: DyscyplDto) {
        return this.dyscyplService.getDyscyplByValue(params);
    }


    @Get()
    getAll() {
        return this.dyscyplService.getAllDyscypls();
    }


    @Put(':id')
    update(@Param('id') id: number, @Body() DyscyplDto: DyscyplDto) {
        return this.dyscyplService.updateDyscypl(id, DyscyplDto);
    }


    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.dyscyplService.remove(id);
    }
}
