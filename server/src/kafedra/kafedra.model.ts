import { Model, Table, Column, DataType, HasMany } from "sequelize-typescript";
import { Vykladach } from "src/vykaldach/vykaldach.model";


interface KafedraCreationAttrs {
    name: string;
    telefone:string;
}

@Table({ tableName: 'kafedra' })
export class Kafedra extends Model<Kafedra, KafedraCreationAttrs> {
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;
    @Column({ type: DataType.STRING, unique: true })
    name: string;
    @Column({ type: DataType.STRING })
    telefone: string;
    @HasMany(() => Vykladach)
    vykladachs: Vykladach[];
}
