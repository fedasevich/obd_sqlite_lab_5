import { Module } from '@nestjs/common';
import { KafedraService } from './kafedra.service';
import { KafedraController } from './kafedra.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Kafedra } from './kafedra.model';

@Module({
  providers: [KafedraService],
  controllers: [KafedraController],
  imports: [
    SequelizeModule.forFeature([Kafedra])
  ],
  exports: [KafedraService]
})
export class KafedraModule {}
