import { IsString } from "class-validator";
export class KafedraDto {
    @IsString({ message: "Must be string" })
    readonly name: string;
    @IsString({ message: "Must be string" })
    readonly telefone: string;
}
