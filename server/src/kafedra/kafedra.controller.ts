
import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { KafedraDto } from './dto/kafedra.dto';
import { KafedraService } from './kafedra.service';
@Controller('kafedra')
export class KafedraController {
    constructor(private kafedraService: KafedraService) { }
    @Post()
    create(@Body() KafedraDto: KafedraDto) {
        return this.kafedraService.createKafedra(KafedraDto);
    }


    @Get('/:kafedra')
    getByValue(@Param() params: KafedraDto) {
        return this.kafedraService.getKafedraByValue(params);
    }


    @Get()
    getAll() {
        return this.kafedraService.getAllKafedras();
    }


    @Put(':id')
    update(@Param('id') id: number, @Body() KafedraDto: KafedraDto) {
        return this.kafedraService.updateKafedra(id, KafedraDto);
    }


    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.kafedraService.remove(id);
    }
}
