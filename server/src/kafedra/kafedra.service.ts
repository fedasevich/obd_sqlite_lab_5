
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { KafedraDto } from './dto/kafedra.dto';
import { Kafedra } from './kafedra.model';
@Injectable()
export class KafedraService {
    constructor(@InjectModel(Kafedra) private kafedraRepository: typeof Kafedra) { }
    async createKafedra(dto: KafedraDto) {
        const candidate = await this.getKafedraByValue(dto);
        if (candidate) {
            throw new HttpException({ message: 'Already exist' }, HttpStatus.BAD_REQUEST);
        }
        const kafedra = await this.kafedraRepository.create(dto);
        return kafedra;
    }
    async getKafedraByValue(dto: KafedraDto) {
        const kafedra = await this.kafedraRepository.findOne({ where: { ...dto } });
        if (!kafedra) {
            throw new HttpException({ message: 'Wrong data' }, HttpStatus.BAD_REQUEST);
        }
        return kafedra;
    }
    async getAllKafedras() {
        const kafedras = await this.kafedraRepository.findAll();
        return kafedras;
    }
    updateKafedra(id: number, dto: KafedraDto) {
        return Kafedra.update({ ...dto }, { where: { id } });
    }
    remove(id: number) {
        return Kafedra.destroy({ where: { id } });
    }
}
