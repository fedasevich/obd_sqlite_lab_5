import { IsNumber } from "class-validator";
export class ScheduleDto {
    @IsNumber({}, { message: "Must be number" })
    readonly weekDay: number;
    @IsNumber({}, { message: "Must be number" })
    readonly classNumber: number;
    @IsNumber({}, { message: "Must be number" })
    readonly dyscyplId: number;
    @IsNumber({}, { message: "Must be number" })
    readonly studGrupId: number;
    @IsNumber({}, { message: "Must be number" })
    readonly vykladachId: number;
}
