import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { GetGroupScheduleById } from './dto/get-group-schedule-by-id.dto';
import { ScheduleDto } from './dto/schedule.dto';
import { ScheduleService } from './schedule.service';


@Controller('schedule')
export class ScheduleController {
    constructor(private scheduleService: ScheduleService) { }
    @Post()
    create(@Body() ScheduleDto: ScheduleDto) {
        return this.scheduleService.createSchedule(ScheduleDto);
    }


    @Get('/:studGrupId')
    getByGroupId(@Param() params: GetGroupScheduleById) {
        return this.scheduleService.getScheduleByGroupId(params);
    }


    @Get()
    getAll() {
        return this.scheduleService.getAllSchedules();
    }


    @Put(':id')
    update(@Param('id') id: number, @Body() ScheduleDto: ScheduleDto) {
        return this.scheduleService.updateSchedule(id, ScheduleDto);
    }


    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.scheduleService.remove(id);
    }
}
