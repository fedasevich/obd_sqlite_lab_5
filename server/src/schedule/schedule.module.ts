import { Module } from '@nestjs/common';
import { ScheduleService } from './schedule.service';
import { ScheduleController } from './schedule.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Schedule } from './schedule.model';

@Module({
  providers: [ScheduleService],
  controllers: [ScheduleController],
  imports: [
    SequelizeModule.forFeature([Schedule])
  ],
})
export class ScheduleModule {}
