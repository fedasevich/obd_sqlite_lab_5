import { Model, Table, Column, DataType, ForeignKey } from "sequelize-typescript";
import { Dyscypl } from "src/dyscypl/dyscypl.model";
import { StudGrup } from "src/stud-grup/stud-grup.model";
import { Vykladach } from "src/vykaldach/vykaldach.model";


interface ScheduleCreationAttrs {
    weekDay: number;
    classNumber: number;
    dyscyplId: number;
    studGrupId: number;
    vykladachId: number;
}
@Table({ tableName: 'schedule' })
export class Schedule extends Model<Schedule, ScheduleCreationAttrs> {
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;

    @Column({ type: DataType.INTEGER, validate: {
        isNumeric: true,
        customValidator(value: number) {
          if (value < 1 || value > 7) {
            throw new Error('The field must contain a number between 1 and 7');
          }
        },
      },
    })
    weekDay: number;

    @Column({ type: DataType.INTEGER })

    classNumber: number;

    @ForeignKey(() => Dyscypl)
    dyscyplId: number;

    @ForeignKey(() => StudGrup)
    studGrupId: number;

    @ForeignKey(() => Vykladach)
    vykladachId: number;
}
