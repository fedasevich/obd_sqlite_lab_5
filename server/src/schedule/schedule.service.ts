
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { GetGroupScheduleById } from './dto/get-group-schedule-by-id.dto';
import { ScheduleDto } from './dto/schedule.dto';
import { Schedule } from './schedule.model';


@Injectable()
export class ScheduleService {
    constructor(@InjectModel(Schedule) private scheduleRepository: typeof Schedule) { }
    async createSchedule(dto: ScheduleDto) {
        const candidate = await this.checkSchedule(dto);
        const candidateExact = await this.checkExactSchedule(dto);
        console.log(candidate, candidateExact);
        if (candidateExact || candidate) {
            throw new HttpException({ message: 'Already exist' }, HttpStatus.BAD_REQUEST);
        }
        const schedule = await this.scheduleRepository.create(dto);
        return schedule;
    }
    async getScheduleByGroupId(dto: GetGroupScheduleById) {
        const schedule = await this.scheduleRepository.findAll({ where: { ...dto } });
        if (!schedule) {
            throw new HttpException({ message: 'Wrong data' }, HttpStatus.BAD_REQUEST);
        }
        return schedule;
    }
    async getAllSchedules() {
        const schedules = await this.scheduleRepository.findAll();
        return schedules;
    }

    async checkSchedule(dto: ScheduleDto) {
        const candidate = await this.scheduleRepository.findOne({ where: { classNumber: dto.classNumber, studGrupId: dto.studGrupId, weekDay: dto.weekDay } });
        console.log(candidate);
        return candidate !== null;
    }

    async checkExactSchedule(dto: ScheduleDto) {
        const candidate = await this.scheduleRepository.findOne({ where: { ...dto } });
        return candidate !== null;
    }

    async updateSchedule(id: number, dto: ScheduleDto) {
        const candidate = await this.checkSchedule(dto);
        console.log(candidate);
        if (candidate) {
            throw new HttpException({ message: 'Already exist' }, HttpStatus.BAD_REQUEST);
        }
        return Schedule.update({ ...dto }, { where: { id } });
    }
    remove(id: number) {
        return Schedule.destroy({ where: { id } });
    }
}
