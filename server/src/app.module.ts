import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { SequelizeModule } from '@nestjs/sequelize';
import { APP_FILTER } from '@nestjs/core';
import { AllExceptionsFilter } from "./filters/all-exceptions.filter";
import { Dyscypl } from "./dyscypl/dyscypl.model";
import { Posada } from "./posada/posada.model";
import { Kafedra } from "./kafedra/kafedra.model";
import { Schedule } from "./schedule/schedule.model";
import { StudGrup } from "./stud-grup/stud-grup.model";
import { Vykladach } from "./vykaldach/vykaldach.model";
import { ScheduleModule } from "./schedule/schedule.module";
import { DyscyplModule } from "./dyscypl/dyscypl.module";
import { PosadaModule } from "./posada/posada.module";
import { KafedraModule } from "./kafedra/kafedra.module";
import { StudGrupModule } from "./stud-grup/stud-grup.module";
import { VykladachModule } from "./vykaldach/vykaldach.module";


@Module({
    controllers: [],
    providers: [
        {
            provide: APP_FILTER,
            useClass: AllExceptionsFilter,
        }
    ],
    imports: [
        ConfigModule.forRoot({
            envFilePath: '.env'
        }),
        SequelizeModule.forRoot({
            dialect: 'sqlite',
            storage: 'db/lab1.sqlite',
            models: [Dyscypl, Posada, Kafedra, Schedule, StudGrup, Vykladach],
            autoLoadModels: true
        }),

        ScheduleModule,
        DyscyplModule,
        PosadaModule,
        KafedraModule,
        ScheduleModule,
        StudGrupModule,
        VykladachModule
    ]
})
export class AppModule { }
