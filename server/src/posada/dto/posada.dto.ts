import { IsNumber, IsString } from "class-validator";
export class PosadaDto {
    @IsString({ message: "Must be string" })
    readonly posada: string;
    @IsNumber({}, { message: "Must be number" })
    readonly norma: number;
}
