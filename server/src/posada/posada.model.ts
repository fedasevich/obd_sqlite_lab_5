import { Model, Table, Column, DataType, HasMany } from "sequelize-typescript";

import { Vykladach } from "src/vykaldach/vykaldach.model";


interface PosadaCreationAttrs {
    privz: string;
    norma: number;
}

@Table({ tableName: 'posada' })
export class Posada extends Model<Posada, PosadaCreationAttrs> {
    @Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
    id: number;
    @Column({ type: DataType.STRING, unique: true })
    posada: string;
    @Column({ type: DataType.INTEGER })
    norma: number;
    @HasMany(() => Vykladach)
    vykladachs: Vykladach[];
}
