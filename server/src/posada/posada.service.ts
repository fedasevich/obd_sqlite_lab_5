
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { PosadaDto } from './dto/posada.dto';
import { Posada } from './posada.model';
@Injectable()
export class PosadaService {
    constructor(@InjectModel(Posada) private posadaRepository: typeof Posada) { }
    async createPosada(dto: PosadaDto) {
        const candidate = await this.getPosadaByValue(dto);
        if (candidate) {
            throw new HttpException({ message: 'Already exist' }, HttpStatus.BAD_REQUEST);
        }
        const posada = await this.posadaRepository.create(dto);
        return posada;
    }
    async getPosadaByValue(dto: PosadaDto) {
        const posada = await this.posadaRepository.findOne({ where: { ...dto } });
        if (!posada) {
            throw new HttpException({ message: 'Wrong data' }, HttpStatus.BAD_REQUEST);
        }
        return posada;
    }
    async getAllPosadas() {
        const posadas = await this.posadaRepository.findAll();
        return posadas;
    }
    updatePosada(id: number, dto: PosadaDto) {
        return Posada.update({ ...dto }, { where: { id } });
    }
    remove(id: number) {
        return Posada.destroy({ where: { id } });
    }
}
