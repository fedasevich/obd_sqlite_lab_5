import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { PosadaDto } from './dto/posada.dto';
import { PosadaService } from './posada.service';

@Controller('posada')
export class PosadaController {
    constructor(private kafedraService: PosadaService) { }
    @Post()
    create(@Body() PosadaDto: PosadaDto) {
        return this.kafedraService.createPosada(PosadaDto);
    }


    @Get('/:kafedra')
    getByValue(@Param() params: PosadaDto) {
        return this.kafedraService.getPosadaByValue(params);
    }


    @Get()
    getAll() {
        return this.kafedraService.getAllPosadas();
    }


    @Put(':id')
    update(@Param('id') id: number, @Body() PosadaDto: PosadaDto) {
        return this.kafedraService.updatePosada(id, PosadaDto);
    }


    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.kafedraService.remove(id);
    }
}
