import { Module } from '@nestjs/common';
import { PosadaService } from './posada.service';
import { PosadaController } from './posada.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Posada } from './posada.model';

@Module({
  providers: [PosadaService],
  controllers: [PosadaController],
  imports: [
    SequelizeModule.forFeature([Posada])
  ],
  exports: [PosadaService]
})
export class PosadaModule {}
